import React from 'react';
import { Link } from 'gatsby';

export default () => (
  <div class="header">
    <h1>Hello Darryl</h1>
    <p>Hello Me!</p>
    <Link to="/">&larr; Back to Home</Link>
  </div>
)
